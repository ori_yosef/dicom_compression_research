import io

import numpy as np
from PIL import Image
from pydicom.encaps import encapsulate
from pydicom.multival import MultiValue


def ensure_even(stream):
    # Very important for some viewers
    if len(stream) % 2:
        return stream + b"\x00"
    return stream


def rescale_pixel_data(raw_pixel_data, rescale_slope, rescale_intercept):
    pixel_data = raw_pixel_data
    pixel_data = np.dot(pixel_data, rescale_slope)
    pixel_data = np.add(pixel_data, rescale_intercept)
    return pixel_data


def rescale_decompressed_pixel_data(raw_pixel_data, rescale_slope, rescale_intercept):
    pixel_data = raw_pixel_data
    pixel_data = np.subtract(pixel_data, rescale_intercept)
    pixel_data = np.divide(pixel_data, rescale_slope)
    return pixel_data


def set_monochrome_values(ds):
    ds.SamplesPerPixel = 1
    ds.BitsAllocated = 8
    ds.BitsStored = 8
    ds.HighBit = 7
    # ds.PixelRepresentation = 0  # PixelRepresntation == 1 -> sign . == 0 -> unsign
    return


def set_rgb_values(ds):
    ds.SamplesPerPixel = 3
    ds.PhotometricInterpretation = "RGB"
    ds.BitsAllocated = 8
    ds.BitsStored = 8
    ds.HighBit = 7
    return


def normilaze_pixel_data_values_by_window(dicom, pixel_data, bits_allocated):
    if isinstance(dicom.WindowWidth, MultiValue):
        dicom_window_width = dicom.WindowWidth[0]
    else:
        dicom_window_width = dicom.WindowWidth
    if isinstance(dicom.WindowCenter, MultiValue):
        dicom_window_center = dicom.WindowCenter[0]
    else:
        dicom_window_center = dicom.WindowCenter

    dicom_half_window_size = (dicom_window_width / 2)
    grayscale_highest_value = 2 ** bits_allocated - 1
    dicom_min_value = dicom_window_center - dicom_half_window_size
    dicom_max_value = dicom_window_center + dicom_half_window_size
    pixel_data = np.where(pixel_data < dicom_min_value, dicom_min_value, pixel_data)
    pixel_data = np.where(pixel_data > dicom_max_value, dicom_max_value, pixel_data)
    pixel_data = ((pixel_data - dicom_min_value) / dicom_window_width) * grayscale_highest_value
    dicom.WindowCenter = 0
    dicom.WindowWidth = 0
    return dicom, pixel_data


def decompress_data_pixel(dicom):

    # set the pydicom parser to extract the compressed image as 16 bit
    orignial_dicom_bits_stored = dicom.BitsStored
    dicom.BitsStored = dicom.BitsAllocated
    # extract image
    array_pixel = dicom.pixel_array
    # reset the dicom bits stored as original
    dicom.BitsStored = orignial_dicom_bits_stored
    # normalization that is required in a specific scenario
    if dicom.BitsAllocated == dicom.BitsStored:
        array_pixel = np.int16(array_pixel + 2 ** dicom.HighBit)
    uncompressed_bytes = array_pixel.tobytes()
    return uncompressed_bytes


def reset_dicom_fields_for_decompressed_dicom(dicom):

    dicom.is_implicit_VR = get_original_values(dicom, 'implicit_VR', bool)
    dicom.file_meta.TransferSyntaxUID = get_original_values(dicom, 'TransferSyntaxUID', str)

    # Need to reset this flag to decompressed dicom
    dicom['PixelData'].is_undefined_length = False
    delattr(dicom, 'LossyImageCompression')
    delattr(dicom, 'LossyImageCompressionMethod')
    delattr(dicom, 'DerivationDescription')


def get_original_values(dicom, lookup_value, value_type):
    original_values = dicom.DerivationDescription
    all_values = original_values.split()
    for value in all_values:
        if lookup_value in value:
            orig_value = value.split("=")[-1]
            if value_type == bool:
                return eval(orig_value)
            else:
                return orig_value
    return False


def jpeg2000_compress_data_pixel(dicom):
    # num = np.frombuffer(dicom.PixelData, dtype=np.uint16).reshape((dicom.Rows, dicom.Columns))
    # image = Image.fromarray(num)
    image = Image.frombytes(mode="I;16", size=(dicom.Rows, dicom.Columns),
                                                    data=dicom.PixelData)  # create image from uint16 pixel
    output = io.BytesIO()
    image.save(output, format="jpeg2000")
    return encapsulate([ensure_even(output.getvalue())])


def set_dicom_fields_for_jpeg2000_compressed_dicom(dicom):
    ################################################
    ############ optional information ##############
    ################################################
    dicom.DerivationDescription = f'The dicom was compressed by lossless JPEG2000 from int16' \
                                  f'original values: dicom.is_implicit_VR={dicom.is_implicit_VR} ' \
                                  f'dicom.file_meta.TransferSyntaxUID={dicom.file_meta.TransferSyntaxUID}'

    ################################################
    ############ mandatory information ##############
    ################################################

    # Need to set this flag to indicate the Pixel Data is compressed
    dicom['PixelData'].is_undefined_length = True
    # TODO - find out what is this flag for
    dicom.is_implicit_VR = False
    # https://dicom.innolitics.com/ciods/ct-image/general-image/00282110
    dicom.LossyImageCompression = '01'  # should be set to '01' if the image was ever compressed
    # https://dicom.innolitics.com/ciods/cr-image/general-image/00282114
    dicom.LossyImageCompressionMethod = 'ISO_15444_1'
    # https://www.dicomlibrary.com/dicom/transfer-syntax/
    dicom.file_meta.TransferSyntaxUID = '1.2.840.10008.1.2.4.90'  # Default Transfer Syntax for Lossy JPEG 12-bit Image Compression


def jpeg_compress_data_pixel(dicom, compression_ratio):
    num = np.frombuffer(dicom.PixelData, dtype=np.int16).reshape((dicom.Rows, dicom.Columns))
    num = rescale_pixel_data(num, float(dicom.RescaleSlope), float(dicom.RescaleIntercept))
    dicom, num = normilaze_pixel_data_values_by_window(dicom, num, 8)
    num = np.uint8(num)             # jpeg compress uint only values
    image = Image.fromarray(num)
    output = io.BytesIO()
    image.save(output, format="jpeg", quality=compression_ratio)
    return encapsulate([ensure_even(output.getvalue())])


def set_dicom_fields_for_jpeg_compressed_dicom(dicom, compression_ratio):
    # Need to set this flag to indicate the Pixel Data is compressed
    dicom['PixelData'].is_undefined_length = True
    # TODO - find out what is this flag for
    dicom.is_implicit_VR = False
    # https://dicom.innolitics.com/ciods/ct-image/general-image/00282110
    dicom.LossyImageCompression = '01'  # should be set to '01' if the image was ever compressed
    # https://dicom.innolitics.com/ciods/cr-image/general-image/00282112
    dicom.LossyImageCompressionRatio = compression_ratio
    # https://dicom.innolitics.com/ciods/cr-image/general-image/00282114
    dicom.LossyImageCompressionMethod = 'ISO_10918_1'
    # https://www.dicomlibrary.com/dicom/transfer-syntax/
    dicom.file_meta.TransferSyntaxUID = '1.2.840.10008.1.2.4.50'  # Default Transfer Syntax for Lossy JPEG 12-bit Image Compression
    ################################################
    ############ optional information ##############
    ################################################
    dicom.DerivationDescription = 'The dicom was compressed lossy JPEG representation from uint8 (originally int16)'


def is_not_a_dicom(dicom_path):
    return not dicom_path[-4:] == '.dcm'
