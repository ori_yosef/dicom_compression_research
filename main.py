import pydicom
import os
import glob
import shutil

from compression_utils import decompress_data_pixel, reset_dicom_fields_for_decompressed_dicom, \
    jpeg2000_compress_data_pixel, set_dicom_fields_for_jpeg2000_compressed_dicom, jpeg_compress_data_pixel, \
    set_dicom_fields_for_jpeg_compressed_dicom, is_not_a_dicom


def compress_lossless_jpeg2000(input_study_dir, output_study_dir, dicom_suffix="*.dcm"):
    all_dicom_instances = prepare_dirs_and_get_dicoms_list(input_study_dir, output_study_dir, dicom_suffix)
    for dicom_instance in all_dicom_instances:
        if os.path.isdir(dicom_instance):
            continue
        dst_path = dicom_instance.replace(os.path.basename(input_study_dir), os.path.basename(output_study_dir))
        os.makedirs(os.path.dirname(dst_path), exist_ok=True)

        dicom = pydicom.read_file(dicom_instance)
        dicom.PixelData = jpeg2000_compress_data_pixel(dicom)
        set_dicom_fields_for_jpeg2000_compressed_dicom(dicom)
        dicom.save_as(dst_path)


def decompress_lossless_jpeg2000(input_study_dir, output_study_dir, dicom_suffix="*.dcm"):
    all_dicom_instances = prepare_dirs_and_get_dicoms_list(input_study_dir, output_study_dir, dicom_suffix)
    for dicom_instance in all_dicom_instances:
        if os.path.isdir(dicom_instance):
            continue
        dst_path = dicom_instance.replace(os.path.basename(input_study_dir), os.path.basename(output_study_dir))
        os.makedirs(os.path.dirname(dst_path), exist_ok=True)

        dicom = pydicom.read_file(dicom_instance)
        dicom.PixelData = decompress_data_pixel(dicom)
        reset_dicom_fields_for_decompressed_dicom(dicom)
        dicom.save_as(dst_path)


def compress_jpeg(input_study_dir, output_study_dir, compression_ratio, dicom_suffix="*.dcm"):
    all_dicom_instances = prepare_dirs_and_get_dicoms_list(input_study_dir, output_study_dir, dicom_suffix)
    for dicom_instance in all_dicom_instances:
        if os.path.isdir(dicom_instance):
            continue
        dst_path = dicom_instance.replace(os.path.basename(input_study_dir), os.path.basename(output_study_dir))
        os.makedirs(os.path.dirname(dst_path), exist_ok=True)

        dicom = pydicom.read_file(dicom_instance)
        dicom.PixelData = jpeg_compress_data_pixel(dicom, compression_ratio)
        set_dicom_fields_for_jpeg_compressed_dicom(dicom, compression_ratio)
        dicom.save_as(dst_path)


def prepare_dirs_and_get_dicoms_list(input_study_dir, output_study_dir, dicom_suffix):
    if os.path.exists(output_study_dir):
        shutil.rmtree(output_study_dir)
    if dicom_suffix:
        return glob.glob(os.path.join(input_study_dir, "**", dicom_suffix), recursive=True)
    else:
        return glob.glob(os.path.join(input_study_dir, "**"), recursive=True)



def test_jpeg_compression():

    compression_ratio = 5
    AN = '1.2.826.0.1.3680043.9.6883.1.13676129125956893525335984781458219'
    # AN = '1.2.826.0.1.3680043.9.6883.1.70184521188135122543357497228231165'
    # AN = '1.2.0'
    dicom_suffix = ''
    output_study_dir = f'/Users/oriyosef/workdir/compression_research/{AN}_jpeg_compressed_not_normalized_{compression_ratio}'
    input_study_dir = f'/Users/oriyosef/workdir/compression_research/{AN}'
    compress_jpeg(input_study_dir, output_study_dir, compression_ratio, dicom_suffix)


def test_jpeg2000_decompression():

    # AN = '1.2.826.0.1.3680043.9.6883.1.70184521188135122543357497228231165'
    AN = '1.2.826.0.1.3680043.9.6883.1.13676129125956893525335984781458219'
    # AN = '1.2.0'
    dicom_suffix = ''
    input_study_dir = f'/Users/oriyosef/workdir/compression_research/{AN}_lossless_jpeg2000_compressed'
    output_study_dir = f'/Users/oriyosef/workdir/compression_research/{AN}_lossless_jpeg2000_decompressed'
    decompress_lossless_jpeg2000(input_study_dir, output_study_dir, dicom_suffix)


def test_jpeg2000_compression():

    # AN = '1.2.826.0.1.3680043.9.6883.1.70184521188135122543357497228231165'
    AN = '1.2.826.0.1.3680043.9.6883.1.13676129125956893525335984781458219'
    # AN = '1.2.0'
    dicom_suffix = ''
    input_study_dir = f'/Users/oriyosef/workdir/compression_research/{AN}'
    output_study_dir = f'/Users/oriyosef/workdir/compression_research/{AN}_lossless_jpeg2000_compressed'
    compress_lossless_jpeg2000(input_study_dir, output_study_dir, dicom_suffix)


if __name__ == '__main__':

    # test_jpeg2000_compression()
    # test_jpeg2000_decompression()
    test_jpeg_compression()