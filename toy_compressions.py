import os

from pydicom import Dataset
from pydicom._storage_sopclass_uids import SecondaryCaptureImageStorage
from pydicom.encaps import encapsulate
from pydicom.uid import generate_uid

from compression_utils import set_rgb_values, ensure_even


def refernce_bob_ross():
    import io
    from PIL import Image, ImageDraw
    from pydicom.dataset import Dataset
    from pydicom.uid import generate_uid
    from pydicom._storage_sopclass_uids import SecondaryCaptureImageStorage

    root_studies_dir = "/Users/oriyosef/workdir/compression_research/reference"
    WIDTH = 100
    HEIGHT = 100
    compression_ratio = 95         # percentage higher is better [1-95]

    def bob_ross_magic():
        image = Image.new("RGB", (WIDTH, HEIGHT), color="red")
        draw = ImageDraw.Draw(image)
        draw.rectangle([10, 10, 90, 90], fill="black")
        draw.ellipse([30, 20, 70, 80], fill="cyan")
        draw.text((11, 11), "Hello", fill=(255, 255, 0))
        return image

    ds = Dataset()
    ds.is_little_endian = True
    ds.is_implicit_VR = True
    ds.SOPClassUID = SecondaryCaptureImageStorage
    ds.SOPInstanceUID = generate_uid()
    ds.fix_meta_info()
    ds.Modality = "OT"
    set_rgb_values(ds)
    ds.BitsAllocated = 8
    ds.BitsStored = 8
    ds.HighBit = 7
    ds.PixelRepresentation = 0
    ds.Rows = HEIGHT
    ds.Columns = WIDTH

    image = bob_ross_magic()
    ds.PixelData = ensure_even(image.tobytes())

    image.save(os.path.join(root_studies_dir, "bob_ross.png"))
    ds.save_as(os.path.join(root_studies_dir, "bob_ross.dcm"), write_like_original=False)  # File is OK

    #
    # Create compressed image
    #
    output = io.BytesIO()
    image.save(output, format="jpeg", quality=compression_ratio)
    ds.PixelData = encapsulate([ensure_even(output.getvalue())])
    # Need to set this flag to indicate the Pixel Data is compressed
    ds['PixelData'].is_undefined_length = True
    # TODO - find out what is this flag for
    ds.is_implicit_VR = False
    # https://dicom.innolitics.com/ciods/ct-image/general-image/00282110
    ds.LossyImageCompression = '01'         # should be set to '01' if the image was ever compressed
    # https://dicom.innolitics.com/ciods/cr-image/general-image/00282112
    ds.LossyImageCompressionRatio = compression_ratio
    # https://dicom.innolitics.com/ciods/cr-image/general-image/00282114
    ds.LossyImageCompressionMethod = 'ISO_10918_1'
    # https://www.dicomlibrary.com/dicom/transfer-syntax/
    ds.file_meta.TransferSyntaxUID = '1.2.840.10008.1.2.4.50'  # Default Transfer Syntax for Lossy JPEG 12-bit Image Compression
    # http://dicom.nema.org/medical/dicom/2017e/output/chtml/part05/sect_8.2.html
    ds.PlanarConfiguration = 0  # RGB=0, MONOCHROME=absent

    ################################################
    ############ optional information ##############
    ################################################
    ds.DerivationDescription = 'The dicom was compressed by JPEG'



    # ds.PhotometricInterpretation = "YBR_FULL_422"
    # ds.file_meta.TransferSyntaxUID = JPEGExtended
    output_dicom_name = "bob_ross_compressed_ratio_"+str(compression_ratio)+"TransferSyntaxUID"+ds.file_meta.TransferSyntaxUID+".dcm"
    ds.save_as(os.path.join(root_studies_dir, output_dicom_name), write_like_original=False)  # File is corrupt


def refernce_random():

    ds = Dataset()
    ds.is_little_endian = True
    ds.is_implicit_VR = True
    ds.SOPClassUID = SecondaryCaptureImageStorage
    ds.SOPInstanceUID = generate_uid()
    ds.fix_meta_info()
    ds.Modality = "OT"
    ds.SamplesPerPixel = 3
    ds.BitsAllocated = 8
    ds.BitsStored = 8
    ds.HighBit = 7
    ds.PixelRepresentation = 0
    ds.PhotometricInterpretation = "RGB"

    import numpy as np
    from PIL import Image
    import io

    # set some parameters
    num_frames = 4
    img_size = 10
    root_studies_dir = "/Users/oriyosef/workdir/compression_research/reference"

    # Create a fake RGB dataset
    random_image_array = (np.random.random((num_frames, img_size, img_size, 3)) * 255).astype('uint8')
    # Convert to PIL
    imlist = []
    for i in range(num_frames):  # convert the multiframe image into RGB of single frames (Required for compression)
        imlist.append(Image.fromarray(random_image_array[i]))

    # Save the multipage tiff with jpeg compression
    f = io.BytesIO()
    imlist[0].save(f, format='tiff', append_images=imlist[1:], save_all=True, compression='jpeg')


    # The BytesIO object cursor is at the end of the object, so I need to tell it to go back to the front
    f.seek(0)
    img = Image.open(f)

    # Get each one of the frames converted to even numbered bytes
    img_byte_list = []
    for i in range(num_frames):
        try:
            img.seek(i)
            with io.BytesIO() as output:
                img.save(output, format='jpeg')
                img_byte_list.append(output.getvalue())
        except EOFError:
            # Not enough frames in img
            break
    ds.PixelData = encapsulate([x for x in img_byte_list])
    ds['PixelData'].is_undefined_length = True
    ds.is_implicit_VR = False
    ds.LossyImageCompression = '01'
    ds.LossyImageCompressionRatio = 10  # default jpeg
    ds.LossyImageCompressionMethod = 'ISO_10918_1'
    ds.file_meta.TransferSyntaxUID = '1.2.840.10008.1.2.4.51'
    ds.PlanarConfiguration = 0
    ds.Rows = img_size
    ds.Columns = img_size
    a = ds.pixel_array
    os.makedirs(root_studies_dir, exist_ok=True)
    ds.save_as(os.path.join(root_studies_dir, "output-reference.dcm"), write_like_original=False)


if __name__ == '__main__':
    refernce_bob_ross()
    refernce_random()
